<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([
            [
                'position_name' => 'Lead'
            ],
            [
                'position_name' => 'Senior'
            ],
            [
                'position_name' => 'Middle'
            ],
            [
                'position_name' => 'Junior'
            ],
            [
                'position_name' => 'Monkey Coder'
            ]
        ]);
    }
}
