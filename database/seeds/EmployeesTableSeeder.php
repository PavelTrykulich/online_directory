<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            [   'parent_id' => 0,
                'surname' => 'Иванов',
                'name' => 'Иван',
                'patronymic' => 'Иванович',
                'date_started_work' => '20.06.2005',
                'salary' => 3000,
                'position' => 1
            ],
            [   'parent_id' => 1,
                'surname' => 'Сергеев',
                'name' => 'Сергей',
                'patronymic' => 'Сергеевич',
                'date_started_work' => '14.04.2009',
                'salary' => 2500,
                'position' => 2
            ],
            [   'parent_id' => 2,
                'surname' => 'Николаев',
                'name' => 'Николай',
                'patronymic' => 'Николаевич',
                'date_started_work' => '14..05.2010',
                'salary' => 2000,
                'position' => 3
            ],
            [   'parent_id' => 3,
                'surname' => 'Димыч',
                'name' => 'Дмитрий',
                'patronymic' => 'Дмитриевич',
                'date_started_work' => '11.11.2011',
                'salary' => 1000,
                'position' => 4
            ],
            [   'parent_id' => 4,
                'surname' => 'Андреев',
                'name' => 'Андрей',
                'patronymic' => 'Андреевич',
                'date_started_work' => '12.12.2012',
                'salary' => 500,
                'position' => 5
            ],
        ]);
    }
}
